import React from 'react';
import CartContext from './CartContext';

export default class GlobalState extends React.Component{
state = {
  Items: [],
}
 
addNewItem = (item) => {
  // eslint-disable-next-line react/no-access-state-in-setstate
  const list = [...this.state.Items, item];
  this.setState({Items: list});
};
 
deleteItem = (itemId) => {
  // eslint-disable-next-line react/no-access-state-in-setstate
  let indexOfItem;
  // eslint-disable-next-line no-plusplus
  for(let i = 0;  i < this.state.Items; i++){
    if(itemId === this.state.Items[i].id){
      indexOfItem = i;
      break;
    }
  }
  // eslint-disable-next-line react/no-access-state-in-setstate
  this.setState(this.state.Items.splice(indexOfItem,1));
};

render(){
 return (
   <CartContext.Provider 
     value={{
        Items: this.state.Items,
        addNewItem: this.addNewItem,
        deleteItem: this.deleteItem
   }}
   >
     {this.props.children}
   </CartContext.Provider>
 );
 }
}