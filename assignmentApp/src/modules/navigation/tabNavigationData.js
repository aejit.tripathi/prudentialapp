import HomeScreen from '../home/HomeViewContainer';
import GridsScreen from '../grids/GridsViewContainer';


const iconHome = require('../../../assets/images/tabbar/home.png');
const cart = require('../../../assets/images/tabbar/cart.png');


const tabNavigationData = [
  {
    name: 'Grids',
    component: GridsScreen,
    icon: iconHome,
    displayName: 'Home',
  },
  {
    name: 'Home',
    component: HomeScreen,
    icon: cart,
    displayName: 'Cart',
  },
];

export default tabNavigationData;