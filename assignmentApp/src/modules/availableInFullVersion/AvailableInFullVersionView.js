import React, { useContext } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Button } from '../../components';
import CartContext from "../../Context/CartContext"

export default function AvailableInFullVersionScreen(props) {

  const { addNewItem } = useContext(CartContext);
  const handleClick = () => {
    addNewItem(props.route.params.article)
    props.navigation.goBack()
  };

  return (
    <View
      style={styles.container}
    >
      <Image
        source={{uri:  props.route.params.article.image}}
        style={styles.ProductImage}
      />

      <View style={styles.textContainer}>
        <Text style={styles.availableText}>{props.route.params.article.title}</Text>
        <Text style={styles.availableText}>{props.route.params.article.subtitle}</Text>
        <Text style={styles.availableText}>{props.route.params.article.price}</Text>
      </View>

      <View style={styles.buttonsContainer}>
        <Button
          large
          secondary
          rounded
          style={styles.button}
          caption="Add To Cart"
          onPress={() => handleClick()}
        />

        <Button
          primary
          large
          bordered
          rounded
          style={styles.button}
          caption="Cancel"
          onPress={() => props.navigation.goBack()}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingVertical: 50,
    justifyContent: 'space-around',
    backgroundColor: 'white',
  },
  ProductImage: {
    width: '100%',
    height: 200,
  },
  availableText: {
    color: colors.black,
    fontFamily: fonts.primaryRegular,
    fontSize: 20,
    marginVertical: 3,
  },
  textContainer: {
    alignItems: 'center',
  },
  buttonsContainer: {
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  button: {
    alignSelf: 'stretch',
    marginBottom: 20,
  },
});
