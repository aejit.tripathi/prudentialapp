import React, { useContext } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import CartContext from '../../Context/CartContext';

const deleteIcon = require('../../../assets/images/tabbar/delete.png')

export default function HomeScreen() {

  const { Items, deleteItem } = useContext(CartContext);

  const handleClick = (item) => {
    deleteItem(item.id)
  }

  let totalPrice = 0;
  // eslint-disable-next-line no-restricted-syntax
  for (const item of Items) {
    // eslint-disable-next-line radix
    totalPrice += parseInt(item?.price.replace(/\$/g, ''))
  };

  const renderItems = ({ item }) => (
    <View
      key={item.id}
      style={styles.itemContainer}
    >
      <View style={styles.itemSubContainer}>
        <Image source={{ uri: item.image }} style={styles.itemImage} />
        <View style={styles.itemContent}>
          <Text style={styles.itemBrand}>{item.brand}</Text>
          <TouchableOpacity onPress={() => handleClick(item)}>
            <Image source={deleteIcon} style={{ width: 20, height: 20, marginHorizontal: 250 }} />
          </TouchableOpacity>
          <View>
            <Text style={styles.itemTitle}>{item.title}</Text>
            <Text style={styles.itemSubtitle} numberOfLines={1}>
              {item.subtitle}
            </Text>
          </View>
          <View style={styles.itemMetaContainer}>
            {item.badge && (
              <View
                style={[
                  styles.badge,
                  item.badge === 'NEW' && { backgroundColor: colors.green },
                ]}
              >
                <Text
                  style={{ fontSize: 10, color: colors.white }}
                  styleName="bright"
                >
                  {item.badge}
                </Text>
              </View>
            )}
            <Text style={styles.itemPrice}>{item.price}</Text>
          </View>
        </View>
      </View>
      <View style={styles.itemHr} />
    </View>
  );

  return (
    <View style={styles.container}>
      {Items.length > 0 && (
        <>
          <FlatList
            data={Items}
            renderItem={renderItems}
          />
          <View style={styles.amountContainer}>
            <Text size={20}>Total Amount Payable</Text>
            <Text size={20}>${totalPrice}</Text>
          </View>
        </>
      )}
      {Items.length === 0 && (
        <View>
          <Text style={styles.noData}>Your Cart is Empty</Text>
        </View>
      )}

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 5,
  },
  itemContainer: {
    backgroundColor: 'white',
    padding: 3,
  },
  itemSubContainer: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  itemImage: {
    height: 100,
    width: 100,
  },
  itemContent: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: 'space-between',
  },
  itemBrand: {
    fontFamily: fonts.primaryRegular,
    fontSize: 14,
    color: '#617ae1',
  },
  itemTitle: {
    fontFamily: fonts.primaryBold,
    fontSize: 16,
    color: '#5F5F5F',
  },
  itemSubtitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 12,
    color: '#a4a4a4',
  },
  itemMetaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemPrice: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
    color: '#5f5f5f',
    textAlign: 'right',
  },
  itemHr: {
    flex: 1,
    height: 1,
    backgroundColor: '#e3e3e3',
    marginRight: -15,
  },
  amountContainer: {
    height: 50,
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  noData: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 70,
    marginVertical: 100,
    width: 500,
    fontWeight: 'bold',
    fontSize: 32,
  }
});
