import React from 'react';

import Navigator from './navigation/Navigator';
import GlobalState from '../Context/GlobalState';

export default function AppView() {
  return (
    <GlobalState>
      <Navigator onNavigationStateChange={() => {}} uriPrefix="/app" />
    </GlobalState>
);
}
