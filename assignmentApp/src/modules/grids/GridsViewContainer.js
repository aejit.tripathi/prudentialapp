import { compose, withState } from 'recompose';

import GridView from './GridsView';


const bookmarkUnselect = require('../../../assets/images/icons/bookmarkUnselect.png');
const bookmarkSelect = require('../../../assets/images/icons/bookmarkSelect.png');
const burgerUnselect = require('../../../assets/images/icons/burgerUnselect.png')
const burgerSelect = require('../../../assets/images/icons/burgerSelect.png')
const drinkUnselect = require('../../../assets/images/icons/drinkUnselect.png')
const drinkSelect = require('../../../assets/images/icons/drinkSelect.png')
const icecreamUnselect = require('../../../assets/images/icons/icecreamUnselect.png')
const icecreamSelect = require('../../../assets/images/icons/icecreamSelect.png')




const listData = [
  {
    id: 1,
    brand: 'Mac Donalds',
    title: 'Combo Pack',
    subtitle: 'Limited time deal',
    price: '$129.99',
    badge: 'NEW',
    badgeColor: '#3cd39f',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSd-L4QNhHXwGv-QLaRvk2CWvmu942Hj_Hnnw&usqp=CAU',
  },
  {
    id: 2,
    brand: 'KFC',
    title: 'Chicken Wings',
    subtitle: 'loremm ipsum',
    price: '$29.99',
    priceFrom: true,
    image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQVFBgUFRUZGRgYHBsbGhoaGBsaGhkbHRgaGxkZGhscIS0kGyIqJBoaJTclKi8xNDQ0GiM6PzozPi0zNDEBCwsLEA8QHxISHzMqIys8MzYzNTMzMzwzNTU9NTUzMTMzMzMzNTw1NTMzMzwzMzM+MzMzMzM1MzM1MzMzMzMzM//AABEIALcBEwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAFAAEDBAYCB//EADsQAAIBAwMCAwcDAgUDBQEAAAECEQADIQQSMUFRBSJhBhMycYGh8JGxwULRFCNSYuFygvEHM1OSohX/xAAaAQACAwEBAAAAAAAAAAAAAAAAAQIDBAUG/8QALREAAgICAQMDAwMEAwAAAAAAAAECEQMhMQQSQRMiUWFxoTKBkRTB0fBCUmL/2gAMAwEAAhEDEQA/APYlNdVEtd0AdUxpCnoAhutQu8skf9QolqDAmqF3EfOq5E4nOyK7SpGWuAKKoDqalR64PSuTTsRb9+ACT0qptZjubr9q6sLuPcD7n/iq/iet2qVUSxxMgAE/z/eq8uRQj3SeiUIOTpC11xUTJ82CPoQazd3XOxdWMYk5AIJwoUkyMkZ+2am/x+XBVidoEkyABJZvv9KGa3UKF94VkI0Pid6wdpxHYTFcfLm9Z/2N+LF2ckY8VuW227gXRxIDlVIgYjdx5Rk961HhXtNYulLe8C44MLBGQTKyesAfOa838a1ltiXtqVwN3VSxEnbuyARBx1B+dceEs9y4l7cQEaegMCME9Z6yTMVp6dyi9CzY4yWz2ma6Wh3hOsF22HBzwR2NXN+a6iZzmiZlqMin3zxTlZpiIC4nbImJicwIkx9R+oroCuXQTJAkYmMx86eaBnc1yTNILXYWgDlVrunAp4oEDdUkMfX+aHuIdT/uH70Z1luYP0P8fnpQ3UWycjkZBrkdXj9zNWKWi/dKOQjjJkiJ6R1GRSTR2cKAAVIIg5B796oaLUvuZnAO3AgQeM/xVjQ6wXGZoAiRznynrWJZotpSSbb8rdfcHCSWuCXWaQ7laZAMkd6o3bjFpB+FgII9ZIq9ZZxdYZKQvyBzJBPoRj0qTWAA+uOnrVOfpoyg8kLW9p/P0HGbtJ7Idfpt7LzP7ep+k/rXWo2ou1fi7AST6ntU9poJJ6AQfz6UPtXkth7zkKGJkkwI6c4FWtQVv/lK9/CX+RK3r4/JzpmO3mMnH1NKhj+O2p8rIR0O8Z+9KuT2P5f5NXY/g1yiuwK4Rqkr3hyxjTmlTMcUAU779Kq3DJqV2qICq5OyaLC8VyoqRBio88D8+dMRWvPnHfFPqNSltdznP+kc/IngVV1niAtyE8zdWPT0Ws3e33WySfzpVEsqiXRxt8lrU+NXbkqnkSeE5PqW5qK47rbVSRzJz5oyf3/er+g0qLiCSInvJz2qjrriKHcOCyyCkAGBye8xBrndVkjKPanbNeCNSutEaalZCheVI3AnGDLEzjv9aynjHiSFWthiPPMqdobH+kjgMO/XjrXOu8UZ1LA7Z4HZcT6g/wB6Epb3j3t0gJnkgbiAcD0PFR6fA7tluSairYR8H0O4G65IU4A7wefl+9ENS4tpNtS8chcwOpMZPyEmgWp8WF1AFaBGQwhhjiOD9KvJrUt2yqyCIAGAc4n+a3rtjoxtufBoPZz2gIc21EHtkgjoeAQfStiniYYZG1vsfka8v8KvtakqoYuZiYJOBz04PNa3Sa1HHluKYwQSFiIn4sHnoaI5kltkZYZLlGr02tABZjAmJPHMfzV5dUs81hX8Rcq9trdxRBCllLWyP9UjkHtTJ4n7uyvnLsR5NxkuSJQ4EwfkeuasWdfsUuJv3O4Y5qENWS0HtFwZle3UHtniiB9p7IguWEmDCkwYkSBnMGrHkVWCg26RoN4HNdq471mk9pLNxlQB9rmA5ACTwBkzz6UU/wAOeNzAdqollm3eOmiTxdv6tBRXpr5ba2yN0HbPE9J9JqCwgHz4nrGYH3P611f1SWwC7KoJAG4xJPAHc1pi323LRU1s60zMyD3i7GI8yyDtbrBHIqq6ZINXxXN63uEjkfeoZsXdH6koSpghHKMVgncRHEZxn5VYfW2rJCASzcKMkyckmldtzQzW6Mk7wfMMg9Qa4WSMscrjz8vwaklPk0A1Q8og+bjHHz7VS1+oAuJbAlmDNzEBYz65IEfOrGiDBJcANHANBr+kLakahXaVG0rgqVzgdjJmfSn1GZ+klk5f04IQiu514DOmsDawzkye4MR/AoBqbMlVmYuoQp4IVwzfZT3+9aTS8HMyef4rPvb2ah3CzJkZzO0CM8cVn6mMY4scvKLMLbbRo9i9h+lKsrd8V1MmFUehzH1kTSrR/X4fhfwL+mn/AKzX7a6FKlFejMQ4qHUPAqaqepOaTehog206LTgVKiVBIkdDihev1fKJ9TVnW3j/AO2vJ5Pah7wAQP8AzUMkvCJ44+WD7iTj79BUTv7sGC3mwCAS0zER/wCKlv3gqliYBMHHaTFVdZpbm5cwCJgQd3bkxOa4vUZHKTS4RvxxVWwirNs3DcOQYz/SI54EA/Wsb7TbgSouZcqTC8wCJ3crwZ7/AFrQ/wD9NlAIUjdEq05mcKeVyeD2oHrrDXi21VUrLAHluTEA8/ETPVRUcaSVk42mZDUIwLJgkfcROMyD+daJWUD2lRgDAwCJAPTHpUNy4ioY83xdODMDPOf5rvTNtIk9J43dJiDia34p9qZXmi5UjjQ+AhrigA8GWjOIz2n7Sa2z2rdpQ7HYNvJWSPKRkgev3JoT4KtwXVOBAAJIYhVKziPkM8ZonrLzB12FbwZgG/pgRImfSTB7VlyuWSWiUIKGij4npFYHO2EUoVkCS0bTOOQcyI6+lPTWHUEbNz/ECB8Wz+oAiI8pE+mIIiqviFxh/mkkpGxJgQyjzbVXgZBkz+tWdPqiFdUfYrhbih1liQVMTGAePlHrU44nFUxeq22khtR4nctN5bgMqCykfCpAmGGQM4Mj702p1RFwK7I9tt5VzZW4fd/FI3DIBVdyypG09ga41ehUTe3sFbaI+I7yMrxBAIIjEeWqOv8AErZtK1pGS5LK6TuVdyw7JyApDbY6Z7VKFaSK5472i/qdd/lpeledtwKCCreYq0GRtIHWM/Oqg8RAw5DKVmYEODkYmR8qHaF2O20WlXaGB4YAEZxMwz8d+tXbiJ03K48xjGZzPXIXdiOIirXxTK7lCVKthBLm4KQSAo+FTkA/DPTpHH71o9Fr0uWha1BbyneroxBG3KmQZBH6Vil1MKiAkwScZG0DJMCCMz9KOXbdw2wbaAggxmCQuSPscfKqacZWjRPa2ej6PVJcUMjBlPBBmYwatMViWA8uZImIHIoB7NJt01tYjng8HcT3ouLxngwOtdKM/am/Jz5RXc0i4G/Pz6V0poe2ut7ym4bgVBB5lgdv6x9jVxHxIq5Oyse7aDfP9/8AmqrpGYmDNXQa6dQef1qnLgjP7kozaBGr8TYiAhkcyYH2qlbvm0AzNu3dIIgdYPT69qNXNJ1GarvpgegrldR0U5S7m9miGSCVUWbd9Np2sCF5jIk55FDtHoG2Brjks53YwFLZ2juM9asWtKoMj/zU0mGBGIEAZPX/AIqOTFKVKUdLgin28Mg/wx/+NT6kn+1NVP8Awt//AORh6bmx/wDqlWX0H/0f4Lb/APS/JpRT1yDXVepMIjVO+KuGq7pOKTGiG2nWpWbaCefTvXZFRXWpVQ+QYiESzfEZ56T1oZ4kx928EfC2T0kcz0jmid95NVL/AMJ+tZsquNF8HTsGa+37u2u1WPCjaJMkDvyZ6+tU7dvUe8T3yf5UECCsMSMgmW9cGOKveEootKoVgq4hiWgg55MgTMfSOlVdc2825YqEOYYsG6KGUcnjnua4akoza5NrjJ+Spr/e3G92bRhG3XHQmFTEHByQAMZYyfWs/p/EWZmKu0b5AIBaOI/2/PIozrNVeIZdyiDBMsvBYA7W5gwD3wKBmyot+8W5DhmBUEjy7WPAGfpWu4tLRXjUu5vwDNcFKkI0uYxwCJ4/kVa8D024mTG1ZHGTmIniMfmCOuIzOlvaf9RPfoT8uKKppWUAjpxIkT9CK1RxtwYTmlJGo8Lvw4syrM2/eqCDbKmSxZsEEE8d8VVskG5/iY3f5h3qWgG2AFEf9BEkekHmoPBtbcuE2tiKwVgzlinA8pWJPEDpzFS3bdu9ZCWyQyOzAuVO9QTAABwZ2kccViku2fFcFveqtkOsRApRtit5sqAzhEXyuHAEy79MAL06XtKLc2i3mATyiFJ3sWZkInB3SCeMnpQjV3ls2jsszcZmV3IJAJ8xifKG69OfSj/hS2xYR1QO5TcznAJySkAE+WCDjvmZiUpSav8AYTrwCtR4Qpk3H2qCWCbvKzTtHmBDfCTgjmgXiyLbP/t7UbjaTt/fExx+3TQa/cSgZlBkq6kLAyWQncPhE7ZMwB1rjxbTMbLi4NigKB037SWZkECFEfnVxfa1Y2YJLrlt1sMrLMMJGT69cTx3otpNO5JdiSzHzEzkg9vp+1RoRbLbwYGQYgDoynsf7ir+k1VvaWlo3cRlRjGK3JxMzTv6lzR6QMp9RjsD3gj7Y696s2tDq1aUdWGfKcCJk4IgfrT3dQ1tA6punkdYo3odWjBTME9Dg4icdYmhQi+ROckr8Bf/ABX+F0qypJS2sAAmSBHQExxmMVU8K9srN47YZG2lm3RsUD4juMSBjp1oibaXbZRgCByCJBQ4I+hoLd9mrezbbVUJjIUGIPAHFWbXBne2ZvU37n+L3KVZHctbNsKDuV12FgeYaDM5mcTj0T2RS4unT3jEk5UEzCESgn5T+sV5/qvD5urZts/lYbmWfKC3LbcYyMwcCeJPqOjthEVQMKAI/wCngfpTxr3Mg0XlrsVGtditBEenIB5pqcUwOTaFcm1UlKo9qCzj3QpV3Sp9qCzqK7WmpxTARqJxUxqJqGAzGRI61VvVPb+Ej1MfKq15qi+CSKTDJqq65qd3qHU4WqGWoqJChiJjsPTtH5xQ21sYksSDBKqVJJJGFI6EeXBzz60a2DZ6kH70OuaE3HAR1BOQSI2nEj/dIH27mubl6ftl3JWbceVNNNgD2n1RQKwksZCiPhBJJk4PQ89z2FYi5rYXJGJEjqeonFaX2g945ZvMbaPtUsSCBJ37Ru6luecfSgvg12x7ydSkoQNpOQvm5ZeuBE/PuaswQVEpyaWg14J4cdi3GxuAOeg/M/WjbWBEAj9a61mu06WwykOkgDZtIByRwcf8UL03jakBbahowG3D6kgAAE4x61rc4x0+DIoyk9ArxQOG2qvmPVTk8ASBmrOjt3LVso5hy24EDzhQsBJ6Tkznt1wSTRhybswDElVwxIwjRk4jvA+3GosPuUjcoUQZBnJIJzEdOxEjHM4Z5oyk6NePG0qZHqFuhRac3FVTvbe8o4ALKCA2TOQMTsPMVBoWawHWSFw4IaAN3Un/AHdfWcc0V0WsW2XVWDs0qXYk7cAfERwACOeepmazWutsbLbFLZILf9DHBk+pJqEJOWmSjBRthu252tcttLkIeBHXaDIzgGI6g/V9WN1ry5PCgwqghSwAnj4TP1OcVmx4oLa7S4G0KBP9QIEZPOZ9PpTajXtchRcWJ2kA/CIgn/T1ImJ64q5Y5N7IuSLWvYAkOisd7KxBJk4IZZx14gc1FrktopuAneoB2qQN5kDOO/NWEfeiNjaARyYLKfibb39fX0qj4hoFuNK5IkcmOZwfSpQai6YpxbVx5IPEde910RRtwSBLQ0kQc4Hp9aIeG+GuxXc5BgA7TzBJK7hxPXAOaHaizcEmWkSNnYDj+RRK1qltpaBwN0PgqVniTwQTtB71d3fBllFp+5G68I1WwgGYnM8wcN+9HlBDEfOft/c1iNN4grsyDOyJPQEmI/f9K2Vi5uCN/qUT84/uDVsHoqkWLWlRX3hRubBPUxJGf1/Wryen50P8UD1/i/urltPdud5BLbTsC5nzcAjHPQ1Zv3rl3T77Eq7orJuEFZAMGeD0+dWxaK2GB0+v71KhqG2TAnJ9Ka3YAcvJ3EARuYqAOyzAPrE1YRLFPTDmnpgPTE04pGgQqVNSpgTClTCnNIY9Q9alrhqAKJvD3uyeEP6yp/vUWqUnAI7kngDuaF6+/t1D57D6QKzP/qH421u2umtmGuDfcIOdp+Ffr/eqHPTL8eNykkibxf24s2WKadBdccu3wA/7QPi/Mmgw/wDUDWbj51jMAIBJ6DMxWMSpKzucvk68OmxxXF/c3nh/t5uIXVWlIP8AXbww9YmD9qP2kXejowa05Oxx0aDCkdD0ivJlNa32c1qpqm0yuW092FkqRDkDa6zxDELPBHeRQ7knF+SnPhjFpwVP8DeI60LFsCfdnLEyMQZEcgnAnsaCs4DJdtg7gS8ACFIaRg8mYPbMRGKP+IoZdNqh1ba3TMkE/r/NZ/U6K8r+Y7GBCDcSDhv6IwVkn0FZoqtMsTjKNoHXnIbdA83mMf1Z3ZAxg9KPeFXCyi0VVRcOXIjYAJWIyMjn1oNr9M++Qf8AulYJHJ7dv7UR0WiZ7iAsQCq5Xczso5eOk8bjAyIBqUo3HZW/a9Bvwy+URLaoWZjx03GRuZsRAnOQTU/iGjb3hUu6q4AIUQm0xAmPLx9qt+FacJbJCbn27TtIwAQMz1IIOOnFdai3mEaSEMbuQcguZ9R6nOKxuVy0SemAb2kdDuRiu0GWUjzDdBEgnzYE4ny1C2nT3bbGbICsu5ijE+gMQft9K1NsKqhfKxKnBI5nIEdzyORBj0qaHQW3LtwgAZBu/pOZMf0jMA5yeIp+o48haZ5t7Q+GFFLAyqwCMnnGCcxOPnVbwy5uQLyeIA5AECfp+tG/bCy5tFgJBYSVIICzI6zPHTrQDwa7AIgeYfk108cnLFZknrJSNN4fKWzgiMsTwARjGDMjnMUQGhYmFad8bABAJJA+KYGTGYqrplLsoJGcCWgCBJmcRg0S0+qMh1G0wevBkmSTgYKjjp1rNP5NKdaRBprK+8cAFNvlIc7z2b4cYzyRVhvdoGUAOhBhdvTpPbIMfSorunZMDJKzJHM9J6kT0Pb0qXSaQuofcBP9MYLQTLHdAkD5+lQ4fddBJJqnsh0Ci25uBztciRiPQ/nrW78GYm0o6qWX6qx/vWDu29w2qOQCMGZjP6wTW39nRhx03yOvxIjfzWzFNSVow5YdroK6nSl7ZVH2EwN4EkCcgTiYxPSZq5p7QRQqiAowPl+CmtDH58jUy/n1rVFGdkijkekVKtRIf7V2OamI7FPUS3V3FJG4CSJyBwCRUlAjoUqanpgKlSpUCJqammuSc0DO5pq5pA0AY32hMX39QP2rz/8A9RGJ19wdBtA+QUR+9ej+1Vn/ADQ3+pf2rEe3uiLC1q1EhlFu5/tdRAJ7SINZJ+fubumklNfYx9uut3SKZBXVziqDsJ6EGgVY0lwqylSQZXjrBBHzyK51GoN1kACqVRUEA+bbgE+pH7UW9k/DTe1VsEyqHe5AwqoZgnuTC/X9HVlfqKKuR6BqNAj6m+WbaVKsIzynmMdYMH50J9orrbLrLb8qbAh5JO5t525G3ygZ7Gi2mRz753Xa1xids8QSQJXnEcHpWY8Q1aozpZGyVCRlQoyzRMY3Ekk5k1iy+7Lfwc+DpJAR3U22QBBeEsSRNtDMFNuQ7jODKiB6wM9ndaU1CXLj43EOzy0r/UT16T6ECpX0ze7dwcGdwDcRwYByD39aA6a9cV0dADseYORKt1GMY4mtsEpRaIz9rR7mmmQiRjdDdMkAQSP6TwOKHarS20S9cuOdwXA3KBAUeRCRMk4kZAcAVY8Hv5W6ZK3gjCMgFh8PykwTzgdqt+IWIJ2ttc+VAQGClv6gGEen16TXHScXv7F0n4vkzmnJKDUX22kkFdiQQWIBLy0RHU9pOAafUaUtcdrIkBthVvgYQGdSQZOWER+xiiCs6KxNsbkliACN6kkSigwCBj6zVVtKq+bYCjBjhvMIMLu6kwyHgCCozgjRuSboI+3RjPGrQdHBwyywAkAMDkHviR96yFl3VjiFHl4Bk4Pz4it14tb222LAAMBBjkA/ESfX9sVnX06yDGREHvj9q3YMi7a8FeWFytcna3GbCkxggRiIO4ZOK0NnWMFi3J2KIWSQSAC/kjjERniayVoNbIn4SxUkgYkBl/Xij9kHbKwCRjJnB4xyDnFPJrgUHafyEzqCfKzMdgCqf6TBgck9TwMQfWrH+KRAxtyVbzwBtZfLBEdYwM/6T0oa9x7dw3AMzO3BC7o8oB9RnEiBU2lto8tCljugfDnbGAsAHzbv+2qZJNWycO7Sf7l/U6m2lxSbbBdu9oBXdvJ5IU8bj5fnPJjUexWo97bZgCE3sEnnaDAk/nFYPXa1gPM5dUBXjlhiJHPMdeuetejewmjNvSpPJG4/NiT/ADWjpYeWZ+oSXkOIsEj87fxXX5+xqW+sMD0Iz6Ecfz+lcEZreZB0/n+P+KkU1Gv5+frXa8n86CgDlLCh2cDzMAG9Y4+tTVxbQDjuT9SZNd0IQ9PTU4pgKlSpUCOi1MzU0U4FIY4FdAUgKi1BYKSgBbseveh6VgC/aSxuRX/0nPyNAbVtSr23XdbcQy/swPRh0NaHSalr9t1upsdfKwJEbiAQVzwZHNBEUgweQYNZptXa8l2N2jEeNexd61L6f/Otc4+NR2ZevzFVNV4Ijp5C1s2wwc3FGy4+zeLaMvmdjDcgDE9Yr0t7aspDEgGMhip9IIzVzQaAJADMUC7QjQwHHEjGBEdZqvst6L5dTPtp/wAnkHgnsnq9RBW0bamJe4Cigd1kS30FeoeCeA29Nb91bk7iC7keZ2HHyUdF9e9HSv2rjVMqqTwSDHep5FGEXJ+BSzzyVEzvi99kS41xwqYVQEO6T2zM8faqFzTW1Rdyk7lMPt3RHJLA4J3RPB44q74hp/fjYwlQ5OCQeAJJ6YkD6VX1htM7IGAFtWLw7KEAAVSomAwxHTOe9cdPiV22aKSSRl9yIpb3QHkIKZBI80yS3mXyhugz35wurcozMuFYxEyPTNabx247XGAOew2YXmCVwTk59BNZ9UOd4lRBz85+1dDDp2wyK1oMeynjNy2QhJYAMFUzCzkgR8+O9en+DeIDU2wzkKyzvXBZoEiAMxDV4za122+ihdqzlmGJ6Njp/etBotW3vluW2O8H6mBkH0PFV58Ku6I4/dGr2jfMhIY8MNoYbgFaXmCCfXGO+elBltbLN7eu9jBX4lIYvtHMA7pHzGDjgzY1gvIHRuZ3AGHRhmIjPXP/ADQr2gANv3isdwC7gG8hBzOD0PeMgRWLDJwlVfQt7VJ7Mz49ZKl7YJYLgHDSAZgQMRkH5GhDORtMBug6zHI9R/ejOv11u2gZbe5txJaSAVYEBTzJkzPbFBtPe8kGJAicHjpH1Nb48a4Iyvg61WlBE7BOGKhsZIjg+v0kVHb95ZlthIQySZYIOhPyOPpV7w9CHK7ZEAcEsDHxCTg55M0T07ORBBAMzjIiR5hEleeJiZ4mk8laIqF7QNseIm5kqSPiaCFnAGSTgY49ZpXNdKlpiTA6E47D4jzJjp61at+CIwICBwCW2kBTgQTgnOTmBO0+tMvgGAfgIH9ZwcZyaFPGnQ6mij4Tpnv3lQkwzyRM+p+sTXuugtBUVRwABXlXsR4cW1BZgRsz2yeK9W07cV0MVeDBlbvZcZJEVUIq6KhvqI3cfzVrRUiFR/H8V0gpgvH53rocGgZ0tPTqKYmgR1SpUhTAVKlSoEdAV0KVMy+pFAzoU9RgtMQIjmevaKkmgCC7a8pCgZ4nAz8s0D8Z0xRxc6NAbtMZ/WtHUOqsB1Kng/b1qrJC1onCVMzeneuW1N4ahEWfdMpkhBKMCCDvJIMwREYrlrbI5RuR9x0NSanWLbtPcPCKzQJkwOJGazRl8l0o+QzbBAgkn1MSf0FA/GbnvGVNoKBwG3YgjIM8zI5q34R4xb1KTbYEqF3AGYkTH0yKoXrrB2jndkRgweR3MH51R187xquCzpo+6/Jx4UAx2gx8UMREkeWfXiT9KzvilnbqF3KTAgs7lw6pywGVURBJgTkRmtLpDGDIJJI7EsYYQODgfkzxY0iK4Zbm6QZVvMRmYU9BJPl+dc6E1FGmSuVsw17TE3IbcgO6N07VXb1aM8j/AO3XihNvQe83yRtXMcZMnyRknIxFej+0Omm2x3BTtKtuIVChy0n+kwPirKamy9vbetHF9YbeMAkQFBwrZJE8SoxWqGS18DTM34j4YvutwuSMQSM9I461HZ0t60QwyIBVvhkf8VshoLTaZXvOwcM4kiFIUcDjdyDIH96BW3DOIMgtCc4GAAx4H75q31JcS2EYRe46H9n/ABs2roNxT5zsYnhQTJYkdiAa2l/SbklXkEMqgnyzgA+XpBOI5NZLWaGQComSFJAEbhJIPfH71rvZ+Bp7YggKAGBkbSD1E4H7D0rNlcXUl9iTi4mO9pw7PxgAQEG0TMlozEj16/Os6FJPPHfH36V6J7R2Nzb8wklxu2rHQCfhPeBB3VivEpe5uWCQ2eRKZg4xuFaMLtJEJPyXNDdM5YSOpwZPQdOn2q9pr7hSoQAyTO7BgHqeMwfpWZ1rvlv6Y44mTnnP0op4LeuIomGXpxMdwwqc8TrXJFZI2H9DMOsk8bVLSFwTEzB5I/Sn12lVkNxC5BedoBkgAEET16fam0eqBYJ5hPOcd8Y/MVokv2bie7jZtIndgmfhOPXtWaalF2yfcvAC9mXuDUoFJhy26eCsSSV6fCD8zzBz6ZZWst7N+DlbrXCTCAqoEck8N1OIP/fWwRYrqdKn22YOpknLRKWgTVe4Zme38GuneflXLDn5f3rSZzpRTIuDzk9+OBjtxUiio0/k/ZzQBKK4jP0qRa4nimB1SFKKUUAKaVKlQIkp6anoAY11TUqBiAp6VImgAX4npluSFI94oBiRMEkCe0wY+RoEj5KsIPBFau1eR13IwYd1II/UVQ8U8M3+ZMOP0b5+tZ8uO9otxz8MzOkSzo2BUlEbcYkxu5M/TuemKs7l81+5cGx/hyQqpA2g9C3xGf8AdFUdVpfeOofcpQzHGaK/4ZLls22EgwQD3GRWWUO+LiW/pdkJ1K7QSRgwpnqQSsg9+/r84p+IWra+dlOQcIYkmCGnqZAgdzxUWl8KcLFxtzbiQWAAUwQoEdpOfvxUur0xUq1wny7SDEiVODzPFczJglB2ro245Rl5JvDdZbvoVvhQVhXLNCziNwY+Uk49YNRe0miBthi/kJUDYqtlgAirOILQZnqTVQuhW55YLk+Xd8Y5kcZnMihml8TOjQptRt0EEEy5zvZwZIOeesDmcTg7XGxSx7dOi5b8LuPut3CZTzDcCA0rEHIieMRx3oTr9E0O9u24mSAF3BRPwyBE4n61rdHrBq7Zv2iVdWKMrCMwJHqCGBziqHi1q4RKPDgw44WRIwGkEZBx6+tOUnF7Hie6Mz4Vda35gCMkbR0kMCc89MmPrWp8DVW3BseRVVpJnYSIg8xAHrmhVoi3aDXAFIBVzEl964UyDtJgHp9OKe/fCW1Adi+7BWVCyQAI6SARE/vRK5aom96D+u0YZWTJ3BVWYjHVu2Os9ayXi/hPxEABVUSRzIaTBHJkR/5FaO37U2HQpcdUuZVwQRkYBVjg45zih3i3iVqCWyygldpRg3EMSOAYE9c08cZxZWr4Zl7mhW4BumQRx6VdS0OKq6PUNdzAE5wCB981qPBPA3uniFHJrqRi/PJkm0no48H8HNxhjAOfQVsD4BZbZjCbcQpB29DIPOOO1XdHpUtIEUf8+pqyq1d6cX+pFPqNcD2xGK6p1WuiKtKjkik35967YUzj8/WmA/X89a5/P/1Xf9/4piP5/egB04/O9cqv7Cuh/f8AemUc0AKKemmnIoENSpYpUwJKVKlSAekDSpUDHqO7bV1KsAVYEEHgg4INSUpoAisWVRQqKFUcBQAB9BUsUqVAFLXeHrdGcMOGHP170BvaZ7J8w8vRhx/xWrrkicGqp4lLa0ycZta8ABHDqT16+vrXFs9+eJ6x2opc8LSdyeU+nH6UrnhwI9fSoemyfejE+N+FXfeNcshW3bS29iSYPwicKsAVZfwpWO4jMDt0+dHLmldT6VEUbJ2mquxW7GtcAvw3w/3LsyKTv5AjjsR/NEtfow+QYAHmI5jPH0J+dMWjkH50wYwdvLd5zziRxWfqOnUlcVsthNp7BGu0I2hcMkAuZy7AZxwDgcdqEay37xlBUHaZDIM9AFiYICgmOs9MUf1Ghc52kGACILAgcGYEfmapP4JfuAi1aRATILbhngGByIJxWeGHJfBqWWCXJiNXYe4SxEBTMwZ9ZPyqN7Nx4t27bMT8TBIwcqB3Jkmea9C0XsOWE6q4Xz8CEog/SDHpWp0HhdqyItoF6TyY+ZzW/H08tORRl6qLVRRmvZj2TFpQ10eY/wBPb51rbVsAQBAqULT7a2KNGFybONtSKKekBUhCAp6UUo60AMPz8/OKXSniqupa6LlsIqlCze8J5UbCVK/9wA+ooEWhSP596RFNQA9IUj+fpSoAVKkaRpgNSoTo/DbyoA11iZYk56sT39aVV98vgYapUqVWCHpUqVIYqY0qVADg09KlQAqVKlQAxp6VKgCNkqMrSpUmM5KDtXK2QOgp6VRGTBBSCAU1KpERRSilSpgOKYilSoEKk0wY56TxPrSpUAOv3roU9KgBUqVKgDiaVKlQAmpUqVMBUzf2pUqAFNKlSoA//9k=',
  },
  {
    id: 3,
    brand: 'Mac Donalds',
    title: 'Burger with Coke',
    subtitle: 'hurry!!, limited time deal',
    price: '$29.99',
    priceFrom: true,
    badge: 'SALE',
    badgeColor: '#ee1f78',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7DrZc9am_038Lq7zG6wzr7EjKPGRqC2FuQA&usqp=CAU',
  },
  {
    id: 4,
    brand: 'Joshis Pizza',
    title: 'Egg Roll',
    subtitle: 'kids special',
    price: '$129.99',
    badge: 'NEW',
    badgeColor: 'green',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1WDdr4AnlVTXP447mRAJBlD-W4kAeH7Ygiw&usqp=CAU',
  },
  {
    id: 5,
    brand: 'Dominos',
    title: 'Pizza',
    subtitle: 'Farm Veg Pizza',
    price: '$29.99',
    priceFrom: true,
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSu5UXrhDuu6uTK6aAPx4_hI3BHhZbT2KDfbQ&usqp=CAU',
    badge: 'NEW',
  },
  {
    id: 6,
    brand: 'Mad Perry',
    title: 'Burger yummy',
    subtitle: 'lorem ipsum',
    price: '$29.99',
    priceFrom: true,
    badge: 'SALE',
    badgeColor: 'red',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2YuI94f9_Tpy3DrJxThtdFuZ61GVRnU1Mz_uHLkh6YE-sYe9oaEBlxFKaTkJXIEFnDFs&usqp=CAU',
  },
  {
    id: 7,
    brand: 'Chatpata',
    title: 'Noodles',
    subtitle: 'Limited time deal',
    price: '$129.99',
    badge: 'NEW',
    badgeColor: '#3cd39f',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSA300aE3NNf7-Gl9mVg9Z0IZj00nSpPEMJTAmhRnER-7Y98qKl2cZ0By7uhPtttvINyvo&usqp=CAU',
  },
  {
    id: 8,
    brand: 'Jayka',
    title: 'Green Salad',
    subtitle: 'lorem ipsum',
    price: '$29.99',
    priceFrom: true,
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeglaXQGDyfvqwT0P7BU0G7cb108KVTx0MeQ&usqp=CAU',
  },
  {
    id: 9,
    brand: 'IDC',
    title: 'Paper Dosa',
    subtitle: 'dummmy text',
    price: '$29.99',
    priceFrom: true,
    badge: 'SALE',
    badgeColor: '#ee1f78',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEXfLwGQVGdlQirjpmtrYTdWPaSQof5WkT7Q&usqp=CAU',
  },
  {
    id: 10,
    brand: 'Pizza Hut',
    title: 'Cheese Pizza',
    subtitle: 'Limited Edition',
    price: '$129.99',
    badge: 'NEW',
    badgeColor: 'green',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5D8e0vFteqPme8oWvB9TZYd0sFNQRkCcV8xPoTY4pm-KPTZEjrxvmLa9QAiCn6xqGAh0&usqp=CAU',
  },
  {
    id: 11,
    brand: 'Dominos',
    title: 'Ham Burger',
    subtitle: 'dummy text',
    price: '$29.99',
    priceFrom: true,
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpcXR356qRab1IQGl2xVoRhYuST2SIOlngPw&usqp=CAU',
  },
  {
    id: 12,
    brand: 'SubWay',
    title: 'Subway Burger',
    subtitle: 'Delicious',
    price: '$29.99',
    priceFrom: true,
    badge: 'SALE',
    badgeColor: 'red',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-z2qIWNCmjoEkYT9eE1l8lb9gMjf6ZkHv0Q&usqp=CAU',
  },
];

const gridIconData = [
  {
    Activeicon: bookmarkSelect,
    InActiveicon: bookmarkUnselect,
    value: 'Grid',
    id: 0,
  },
  {
    Activeicon: burgerSelect,
    InActiveicon: burgerUnselect,
    value: 'List 1',
    id: 1,
  },
  {
    Activeicon: drinkSelect,
    InActiveicon: drinkUnselect,
    value: 'List 2',
    id: 2,
  },
  {
    Activeicon: icecreamSelect,
    InActiveicon: icecreamUnselect,
    value: 'List 3',
    id: 3,
  }
]

export default compose(
  withState('tabIndex', 'setTabIndex', 0),
  withState('tabs', 'setTabs', gridIconData),
  withState('data', 'setData', listData),
)(GridView);
