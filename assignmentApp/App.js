import React from 'react';
import { NavigationContainer } from '@react-navigation/native';


import AppView from './src/modules/AppViewContainer';

export default function App() {
  return (
    <NavigationContainer>
      <AppView />
    </NavigationContainer>
  );
}