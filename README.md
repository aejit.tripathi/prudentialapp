# Prudentialapp

## Getting started

To install app navigate to assignmentApp folder and run 
## yarn install
## cd ios
## pod install

# NOTE if after running images are not rendering then follow bwlow approaches ( either 1 or 2)


```Navigate to node_modules > react-native > Libraries > Images > RCTUIImageViewAnimated.m search for if (_currentFrame)```

1. Add below line for temp fix 
```
if (_currentFrame) {
    layer.contentsScale = self.animatedImageScale;
    layer.contents = (__bridge id)_currentFrame.CGImage;
  } else {
    [super displayLayer:layer];
  }
```

2. Go to your PodFile and replace 
```
post_install do |installer|
  installer.pods_project.targets.each do |target|
    if target.name == "React"
      target.remove_from_project
  end
end
```
with 
```
post_install do |installer|
  installer.pods_project.targets.each do |target|
    if target.name == "React"
      target.remove_from_project
    end
  end
  find_and_replace("../node_modules/react-native/Libraries/Image/RCTUIImageViewAnimated.m",
    "_currentFrame.CGImage;","_currentFrame.CGImage ;} else { [super displayLayer:layer];")
end

def find_and_replace(dir, findstr, replacestr)
  Dir[dir].each do |name|
      text = File.read(name)
      replace = text.gsub(findstr,replacestr)
      if text != replace
          puts "Fix: " + name
          File.open(name, "w") { |file| file.puts replace }
          STDOUT.flush
      end
  end
  Dir[dir + '*/'].each(&method(:find_and_replace))
end
```
